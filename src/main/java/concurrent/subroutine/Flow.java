package concurrent.subroutine;

import java.util.function.Consumer;
import java.util.function.Predicate;

import concurrent.Task;

public class Flow<C> {
	private final C context;
	
	private Subroutine<C> start = null;
	
	private Subroutine<C> current = null;
	
	private Subroutine<C> and = null;
	
	private Subroutine<C> or = null;
	
	private boolean predicate = false;
	
	private Flow(C context) {
		this.context = context;
	}
	
	public Flow<C> then(Consumer<C> f) {
		if(f == null)
			throw new NullPointerException();
		
		if(start == null)
			start = current = new Subroutine<>(f, context);
		else {
			if(predicate) {
				if(and == null) {
					and = current.and(new Subroutine<>(f, context));
				} else {
					and.then(start);
					if(or != null)
						or.then(start);
					current = current.then(new Subroutine<>(f, context));
					and = or = null;
					predicate = false;
				}
			} else
				current = current.then(new Subroutine<>(f, context));	
		}
		return this;
	}
	
	public Flow<C> and(Consumer<C> f) {
		test(f);
		if(predicate) {
			if(and == null)
				and = current.and(new Subroutine<>(f, context));
			else {
				and = and.then(new Subroutine<>(f, context));
			}
		} else
			current = current.and(new Subroutine<>(f, context));
		return this;
	}
	
	public Flow<C> or(Consumer<C> f) {
		test(f);
		if(!predicate)
			throw new RuntimeException();
		if(or == null)
			or = current.or(new Subroutine<>(f, context));
		else
			or = or.then(new Subroutine<>(f, context));
		return this;
	}
	
	public Task loop() {
		if(predicate) {
			predicate = false;
			if(and == null)
				throw new RuntimeException();
			and.then(start);
			if(or != null)
				or.then(start);
			current.then(start);
			and = or = null;
		} else
			current.then(start);
		return start;
	}

	public Flow<C> predicate(Predicate<C> p) {
		test(p);
		if(predicate)
			throw new RuntimeException();
		current = current.then(new Subroutine<>(p, context));
		predicate = true;
		return this;
	}
	
	public Task build() {
		return start;
	}

	public static <T> Flow<T> context(T context) {
		return new Flow<T>(context);
	}
	
	private final void test(Object t) {
		if(t == null || current == null)
			throw new NullPointerException();
	}
	
	
	
}
