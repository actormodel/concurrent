package concurrent.subroutine;

import java.util.function.Consumer;
import java.util.function.Predicate;

import concurrent.Task;

class Subroutine<C> extends Task {
	protected final C context;

	private final Consumer<C> f;

	private final Predicate<C> p;
	
	protected Subroutine<C> next;
	
	protected Subroutine<C> and;
	
	protected Subroutine<C> or;
	
	Subroutine(Predicate<C> p, C context) {
		this.context = context;
		this.p = p;
		f = null;
	}
	
	Subroutine(Consumer<C> f, C context) {
		this.context = context;
		this.f = f;
		p = null;
	}
	
	public Subroutine<C> then(Subroutine<C> s) {
		return next = s;
	}
	
	public Subroutine<C> and(Subroutine<C> s) {
		return and = s;
	}
	
	public Subroutine<C> or(Subroutine<C> s) {
		return or = s;
	}
	
	@Override
	public void execute() {
		if(p != null) {
			if(p.test(context)) {
				if(and != null)
					getWorker().dispatch(and);
				else
					throw new RuntimeException();
			} else
				if(or != null)
					getWorker().dispatch(or);
				else if(next != null)
					getWorker().dispatch(next);
		} else {
			f.accept(context);
			if(next != null)
				getWorker().dispatch(next);
		}
	}
	
}
