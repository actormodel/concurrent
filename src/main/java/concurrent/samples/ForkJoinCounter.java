package concurrent.samples;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.TimeUnit;

public class ForkJoinCounter extends ForkJoinTask<Void> {
	private long count = 0;
	
	private long time = System.currentTimeMillis() + 1000;
	
	private void print() {
		System.out.println(count);
	}
	
	private void inc() {
		count++;
	}
	
	private boolean isTime() {
		return time <= System.currentTimeMillis();
	}
	
	private void newTime() {
		time = System.currentTimeMillis() + 1000;
	}
	
	private void reset() {
		count = 0;
	}
	
	private void tick() {
		inc();
		if(isTime()) {
			print();
			newTime();
			reset();
		}
	}
	
	@Override
	public Void getRawResult() { return null; }

	@Override
	protected void setRawResult(Void value) { }

	@Override
	protected boolean exec() {
		tick();
		ForkJoinPool.commonPool().execute(this);
		return false;
	}

	private static final long serialVersionUID = 5044531020368465272L;		
	
	public static void main(String...args) {
		ForkJoinPool.commonPool().execute(new ForkJoinCounter());
		ForkJoinPool.commonPool().awaitQuiescence(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
	}
}
