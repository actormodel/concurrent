package concurrent.samples;

import concurrent.Task;
import concurrent.WorkerService;

public class WorkerCounter extends Task {
	private long count = 0;
	
	private long time = 0;
	
	private final int index;
	
	WorkerCounter(int index) {
		this.index = index;
	}
	
	{ newTime(); }
	
	private final void inc() {
		count++;
	}
	
	private final boolean isTime() {
		return time <= System.currentTimeMillis();
	}
	
	private final void print() {
		System.out.println(getWorker() + " " + index + ":\t" + count);
	}
	
	private final void reset() {
		count = 0;
	}
	
	private final void newTime() {
		time = System.currentTimeMillis() + 1000;
	}
	
	@Override
	public void execute() {
		inc();
		if(isTime()) {
			print();
			reset();
			newTime();
		}
		repeat(true, false);
	}

	public static void main(String...args) {
		System.setProperty("WorkerService.queueType", "linked");
		for(int i = 0 ; i < 1 ; i++)
			WorkerService.execute(new WorkerCounter(i));
	}
}
