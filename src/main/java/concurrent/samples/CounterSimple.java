package concurrent.samples;

public class CounterSimple extends Thread {
	
	@Override
	public void run() {
		long time = System.currentTimeMillis() + 1000;
		long count = 0;
		while(true) {
			count++;
			if(time < System.currentTimeMillis()) {
				System.out.println(count);
				count = 0;
				time = System.currentTimeMillis() + 1000;
			}
		}
	}
	
	public static void main(String...args) {
		new CounterSimple().start();
	}
	
}
