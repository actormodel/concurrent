package concurrent.samples;

import concurrent.WorkerService;
import concurrent.subroutine.Flow;

public class FlowCounter {
	public long count = 0;
	
	public long time = System.currentTimeMillis() + 1000;
	
	public static void main(String...args) {
		WorkerService.execute(
			Flow.context(new FlowCounter())
			.then(FlowCounter::inc)
			.predicate(FlowCounter::test)
			 .then(FlowCounter::print)
			  .and(FlowCounter::reset)
			  .and(FlowCounter::newTime)
			.loop()
		);
		
		/*
		 * WorkerService.execute(
		 * 	Flow.context(new FlowCounter())
		 *  .then(ctx -> ctx.count++)
		 *  .predicate(ctx -> ctx.time <= System.currentTimeMillis())
		 *   .then(ctx -> System.out.println("print"))
		 *    .and(ctx -> ctx.count = 0)
		 *    .and(ctx -> ctx.time = System.currentTimeMillis() + 1000)
		 *   .loop()
		 * )
		 * 
		 * WorkerService.execute(
		 * 	Flow.context(new FlowCounter())
		 *  .then("inc")
		 *  .predicate("isTime")
		 *   .then("print")
		 *    .and("reset")
		 *    .and("newTime")
		 *  .loop()
		 * )
		 */
	}
	
	public static void print(FlowCounter ctx) {
		System.out.println(ctx.count);
	}
	
	public static void inc(FlowCounter ctx) {
		ctx.count++;
	}
	
	public static boolean test(FlowCounter ctx) {
		return ctx.time <= System.currentTimeMillis();
	}
	
	public static void reset(FlowCounter ctx) {
		ctx.count = 0;
	}
	
	public static void newTime(FlowCounter ctx) {
		ctx.time = System.currentTimeMillis() + 1000;
	}
}
