package concurrent.samples;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ExecutorCounter implements Runnable {
	private static Executor executor;
	
	private long time;
	
	private long count;
	
	{ newTime(); }
	
	public void inc() {
		count++;
	}
	
	public boolean isTime() {
		return time < System.currentTimeMillis();
	}
	
	public void print() {
		System.out.println(count);
	}
	
	public void reset() {
		count = 0;
	}
	
	public void newTime() {
		time = System.currentTimeMillis() + 1000;
	}
	
	@Override
	public void run() {
		inc();
		if(isTime()) {
			print();
			newTime();
			reset();
		}
		executor.execute(this);
	}
	
	public static void main(String...args) {
		(executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())).execute(new ExecutorCounter());
	}
	
}
