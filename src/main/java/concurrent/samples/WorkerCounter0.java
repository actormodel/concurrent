package concurrent.samples;

import concurrent.Task;
import concurrent.WorkerService;

/*
 * self repeate
 */
public class WorkerCounter0 extends Task {
	private long count = 0;
	
	private long time = 0;
	
	{ newTime(); }
	
	private final void inc() {
		count++;
	}
	
	private final boolean isTime() {
		return time <= System.currentTimeMillis();
	}
	
	private final void print() {
		System.out.println(count);
	}
	
	private final void reset() {
		count = 0;
	}
	
	private final void newTime() {
		time = System.currentTimeMillis() + 1000;
	}
	
	@Override
	public void execute() {
		inc();
		if(isTime()) {
			print();
			reset();
			newTime();
		}
		repeat(true, true);
	}

	public static void main(String...args) {
		System.setProperty("WorkerService.queueType", "linked");
		WorkerService.getService();
		WorkerService.execute(new WorkerCounter0());
	}
}
