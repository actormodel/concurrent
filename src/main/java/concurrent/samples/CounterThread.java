package concurrent.samples;

public class CounterThread extends Thread{
	private long count = 0;
	
	private long time;
	
	public void newTime() {
		time = System.currentTimeMillis() + 1000;
	}
	
	public boolean isTime() {
		return time < System.currentTimeMillis();
	}
	
	public void inc() {
		count++;
	}
	
	public void print() {
		System.out.println(count);
	}
	
	public void reset() {
		count = 0;
	}
	
	@Override
	public void run() {
		newTime();
		while(true) {
			inc();
			if(isTime()) {
				print();
				reset();
				newTime();
			}
		}
	}
	
	public static void main(String...args) {
		new CounterThread().start();
	}
	
}
