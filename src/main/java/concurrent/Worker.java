package concurrent;

import java.util.concurrent.atomic.AtomicInteger;

import concurrent.collection.Queue;

public class Worker implements Runnable {
	final WorkerService service;
	
	final Thread thread;
	
	private final Queue queue;
	
	private final Channel ch;
	
	private final int index;
	
	private final Balancer balancer;
	
	Worker(int index, WorkerService service, Channel ch, Balancer balancer) {
		this.index = index;
		this.service = service;
		this.ch = ch;
		queue = service.newQueue();
		thread = new Thread(this);
		this.balancer = balancer;
	}
	
	private Task current;
	
	private Task next() {
		return current = queue.next();
	}
	
	private long startTime;
	
	private long delay;
	
	public void dispatch(Task task) {
		if(!ch.execute(index, balancer.next(), task))
			queue.add(task);
	}
	
	@Override
	public void run() {
		while(true) {
			startTime = System.currentTimeMillis();
			if(delay < startTime) {
				if(lock.get() == 1) {
					queue.add(outer);
					outer = null;
					lock.set(0);	
				}
				delay = startTime + 100;
			}
			for(int tick = 0 ; tick < 10 ; tick++) {
				if(next() == null)
					break;
				current.execute(this);
				if(current.repeatable) {
					if(current.self) {
						queue.add(current);
					} else
						dispatch(current);
				}
			}
			ch.flush(index, queue);
		}
	}
	
	private final AtomicInteger lock = new AtomicInteger(0);
	
	private volatile Task outer = null;
	
	final void execute(Task task) {
		while(!lock.compareAndSet(0, 1))
			Thread.yield();
		outer = task;
	}
	
	@Override
	public String toString() {
		return "Worker #" + index;
	}
	
}
