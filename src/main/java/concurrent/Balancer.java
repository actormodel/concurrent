package concurrent;

class Balancer {
	private final int count;
	
	private int index = 0;
	
	public Balancer(int count, int start) {
		if(start > count || start < 0)
			throw new RuntimeException();
		this.count = count;
		index = start;
	}
	
	public int next() {
		if(index == count)
			index = 0;
		return index++;
	}
}
