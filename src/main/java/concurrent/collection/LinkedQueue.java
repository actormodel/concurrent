package concurrent.collection;

import concurrent.Task;

public class LinkedQueue implements Queue {
	private Entry head;
	
	private Entry tail;
	
	private int count;
	
	@Override
	public final void add(Task value) {
		if(value == null)
			throw new NullPointerException();
		if(isEmpty()) 
			head = tail = new Entry(value, null);
		else
			tail = tail.prev = new Entry(value, tail);
		count++;
	}

	@Override
	public final void add(Queue queue) {
		if(queue == null)
			throw new NullPointerException();
		add((LinkedQueue) queue);
	}
	
	private final void add(LinkedQueue queue) {
		if(queue.isEmpty())
			return;
		if(isEmpty()) {
			head = queue.head;
			tail = queue.tail;
			count = queue.count;
		} else {
			tail.prev = queue.head;
			queue.head.next = tail;
			tail = queue.tail;
			count += queue.count;
		}
		queue.head = null;
		queue.tail = null;
		queue.count = 0;
	}
	
	@Override
	public final Task next() {
		if(isEmpty())
			return null;
		final Task value = head.value;
		count--;
		head = head.prev;
		if(head != null) {
			head.next.prev = null;
			head.next = null;
		} else
			tail = null;
		return value;
	}

	@Override
	public final boolean isEmpty() {
		return count == 0;
	}	
	
	static class Entry {
		final Task value;
		
		Entry next;
		
		Entry prev;
		
		Entry(final Task v, final Entry n) { value = v; next = n; }
	}
}
