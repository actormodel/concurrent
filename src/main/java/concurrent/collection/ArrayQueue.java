package concurrent.collection;

import concurrent.Task;

public class ArrayQueue implements Queue {
	private static final int SIZE = 2 << 2;
	
	Task[] array = new Task[SIZE];

	//current task count in array
	int count = 0;//not 
	
	int gindex = 0;//get index
	
	int aindex = 0;//add index
	
	int capacity = array.length;
	
	@Override
	public final void add(Task value) {
		if(value == null)
			throw new NullPointerException();
		if(count == capacity)
			grow();
		array[aindex++] = value;
		if(aindex == capacity)
			aindex = 0;
		count++;	
	}

	@Override
	public final void add(Queue queue) {
		if(queue == null)
			throw new NullPointerException();
		add((ArrayQueue) queue);
	}
	
	private final void add(ArrayQueue queue) {
		while(!queue.isEmpty())
			add(queue.next());
	}

	@Override
	public Task next() {
		if(isEmpty())
			return null;
		if(gindex == capacity)
			gindex = 0;
		count--;
		Task task = array[gindex];
		array[gindex++] = null;
		if(isEmpty())
			reset();
		return task;	
	}
	
	@Override
	public final boolean isEmpty() {
		return count == 0;
	}

	private final void reset() {
		count = aindex = gindex = 0;
	}
	
	/* aindex < gindex
	 * 0 :			0: aindex	0: aindex	0:		 	0:
	 * 1 : aindex	1:			1:			1: aindex	1: aindex
	 * 2 :			2:			2:			2:			2: gindex
	 * 3 : gindex	3:			3: gindex	3:			3:
	 * 4 : 			4: gindex	4:			4: gindex	4:
	 * 
	 * aindex > gindex
	 * 
	 * 0 :
	 * 1 : gindex
	 * 2 :
	 * 3 : aindex
	 * 4 :
	 */
	private final void grow() {
		final Task[] newArray = new Task[array.length << 1];
		int n = count;
		for(int i = 0 ; i < n ; i++)
			newArray[i] =  next();
		count = n;
		aindex = n;
		gindex = 0;
		capacity = newArray.length;
		array = newArray;
	}
}
