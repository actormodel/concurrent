package concurrent.collection;

import concurrent.Task;

public interface Queue {

	public void add(Task value);
	
	public void add(Queue queue);

	public Task next();
	
	public boolean isEmpty();
}
