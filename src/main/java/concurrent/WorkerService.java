package concurrent;

import concurrent.collection.ArrayQueue;
import concurrent.collection.LinkedQueue;
import concurrent.collection.Queue;

public class WorkerService {
	private static WorkerService service;

	private final Worker[] workers;
	
	private WorkerService(int parallelism, QueueFactory factory) {
		if(factory == null)
			throw new NullPointerException();
		this.factory = factory;
		workers = new Worker[parallelism];
		final Channel ch = new Channel(parallelism, this);
		for(int i = 0 ; i < parallelism ; i++)
			(workers[i] = new Worker(i, this, ch, new Balancer(parallelism, i + 1))).thread.start();
	}
	
	public static WorkerService getService(){
		return service;
	}
	
	public static void execute(Task task) {
		getService().workers[0].execute(task);
	}
	
	static {
		int parallelism = Runtime.getRuntime().availableProcessors() - 1;
		if(parallelism <= 0)
			parallelism = 1;
		final String queueType = System.getProperty("WorkerService.queueType");
		QueueFactory factory = null;
		if(queueType != null) {
			switch(queueType) {
			case "linked": factory = new LinkedQueueFactory(); break;
			case "array": factory = new ArrayQueueFactory(); break;
			default: factory = new LinkedQueueFactory(); break;
			}
		} else
			factory = new LinkedQueueFactory();
		service = new WorkerService(parallelism, factory);
	}

	private static interface QueueFactory {
		Queue create();
	}
	
	private static class LinkedQueueFactory implements QueueFactory {
		@Override
		public Queue create() {
			return new LinkedQueue();
		}
	}
	
	private static class ArrayQueueFactory implements QueueFactory {
		@Override
		public Queue create() {
			return new ArrayQueue();
		}
	}
	
	private final QueueFactory factory;
	
	public Queue newQueue() {
		return factory.create();
	}
}
