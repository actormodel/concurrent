package concurrent;

public abstract class Task {
	private Worker worker;
	
	private boolean stoped = false;
	
	protected void stop() {
		stoped = true;
	}
	
	protected void before() { }
	
	public abstract void execute();

	protected void after() { }
	
	final void execute(Worker worker) {
		repeatable = false;
		self = false;
		setWorker(worker);
		try {
			before();
			if(!stoped) {
				execute();	
				after();
			}
		} catch(Throwable e) { error(e); }
		setWorker(null);
	}
	
	protected void error(Throwable e) {
		e.printStackTrace();
	}
	
	private final void setWorker(Worker worker) {
		this.worker = worker;
	}
	
	boolean repeatable = false;
	
	boolean self = false;
	
	protected final void repeat(boolean state, boolean self) {
		repeatable = state;
		this.self = self;
	}
	
	protected final Worker getWorker() {
		return worker;
	}
}
