package concurrent;

import concurrent.collection.Queue;

final class Channel {
	private final WorkerService service;
	
	private final Pipe[] pipes;
	
	private final int count;
	
	Channel(int count, WorkerService s) {
		service = s;
		this.count = count;
		pipes = new Pipe[count*count];
		for(int source = 0 ; source < count ; source++)
			for(int target = 0 ; target < count ; target++)
				pipes[source*count + target] = new Pipe(source, target);
	}

	boolean execute(int source, int target, Task task) {
		if(source == target)
			return false;
		pipes[source*count + target].write(task);
		return true;
	}
	
	void flush(int target, Queue list) { 
		for(int i = 0 ; i < count ; i++) {
			pipes[i*count + target].flush(list);
			pipes[target*count + i].swap();
		}
	}
	
	final class Pipe {
		final int source;
		
		final int target;
		
		Queue out = service.newQueue();
		
		Queue temp;
		
		Queue buffer = service.newQueue(); 

		volatile int hasOut = 0;
		
		volatile int swap = 0;
		
		Pipe(int s, int t) {
			source = s;
			target = t;
		}
		
		//call from thread source
		void write(Task task) {
			out.add(task);
			hasOut = 1;
		}

		//call from thread source
		void swap() {
			if(hasOut == 1 && swap == 1) {
				temp = out;
				out = buffer;
				buffer = temp;
				hasOut = 0;
				swap = 0;
			}
		}
		
		void flush(final Queue list) {
			if(swap == 0) {
				list.add(buffer);
				swap = 1;
			}
		}
	}
}
